﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    public float TimeLeft = 185f;
    public bool gameOver;
    public Text TimerText;

    private void Start()
    {
        gameOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Subtracts time past from TimeLeft
        TimeLeft -= Time.deltaTime;
        //sets timer text
        TimerText.text = "Time Left: " + Mathf.Floor(TimeLeft);
        if(TimeLeft < 0)
        {
            gameOver = true;
            Time.timeScale = 0;
        }

        //Allows user to reset scene whenever they hit space
        if (Input.GetKeyDown(KeyCode.Space) && gameOver == true)
        {
            GameOver();
            Time.timeScale = 1;
        }
    }

    void GameOver()
    {
        //reloads scene on timer running out
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
