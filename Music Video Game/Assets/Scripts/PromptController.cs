﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PromptController : MonoBehaviour
{
    //creates array for all Crates to be stored. 0 = blue, 1 = green, 2 = red, 3 = yellow.
    public GameObject[] Crates;
    public Text PromptText;
    public float Points = 0f;
    public Text PointText;

    //creates two gameobjects to store the randomly selected crates as
    private GameObject Crate1;
    private GameObject Crate2;

    private void Start()
    {
        Reroll();
    }

    private void Update()
    {
        //checks to see if Crate1 and Crate2 are touching
        if(Crate1.GetComponent<CrateController>().OtherColor == Crate2.tag)
        {
            Points += 1;
            Reroll();
            print(Points);
            PointText.text = "Points: " + Points;
        }
    }

    //creates new random prompt
    void Reroll()
    {
        Crate1 = Crates[Mathf.FloorToInt(Random.Range(0, 4))];
        bool NewCrate = false;
        //keeps choosing new crate 2 until crate 2 is not the same as crate 1
        while(NewCrate == false)
        {
            Crate2 = Crates[Mathf.FloorToInt(Random.Range(0, 4))];
            if(Crate1 != Crate2)
            {
                //stops loop, sets text to new prompt
                NewCrate = true;
                PromptText.text = "Make " + Crate1.tag + " and " + Crate2.tag + " touch!";
            }
        }
    }
}
